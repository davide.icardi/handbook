![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---


The Agile Lab handbook is the central repository for how we run the company. Please make a merge request to suggest improvements or add clarifications. Please use issues to ask questions.

**Table of Contents** 

*   [Introduction](Introduction.md)
*   [Company](Company.md)
*   [People Operations](PeopleOperations.md)
*   [Engineering](Engineering.md)