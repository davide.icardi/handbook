
# Estimation and Planning

Often that estimate determines the pricing and consequently also the amount of "days" that can be used to carry out the project. 

However, at the beginning of each project, a second estimate is made and it is in charge of the Project Leader and reviewed by the COO for acceptance. Based on this estimate (which may differ from the first, if accepted by the COO) and project planning, the people needed to build up the project team and ensure the delivery are allocated.

Project timelines are often also defined during pre-sales as a strong requirement by the customer to accept the project. If, on the other hand, they can be decided in common agreement with the customer, the COO will care to make them as compatible as possible with the general delivery plan.


# Team Organization

Each project or product will be a circle in which the following roles may be present:

*   Project Leader - mandatory
*   Developer - mandatory
*   Architect - mandatory
*   Devops - optional
*   PMO - optional
*   Account - optional


# Project lifecycle

It is important to understand that not all the customers are embracing Agile methodology, that is our preferred way to manage a project. 
Then towards the customer we expose a very simple lifecycle:
*   Design
*   Implementation
*   User acceptance Test
*   Deployment  

While internally we adopt Agile with fine grained iterations as described in [Software Development](SoftwareDevelopment.md) section.

We also categorize projects in two types:
- **Formal**: the customer requires an end to end solution released in production and with full visibility on deliverables. Customer also produces detailed requirements and both functional and technical analysis are required.
- **Informal**: requirements are leaking, we have to deliver quick win to the customer and the process is clearly iterative.


# Design

The Design phase of a project or project iteration has a mandatory involvement of the Project Leader and the Architect, and optional involvement of the Developer (at the discretion of the Project Leader).
The deliverables needed at this stage are:
- High-Level Design ( [HLD](HLD.md) )
- Functional Analysis (If the complexity of the problem requires it and we are in the context of a Formal project, this tends to be provided by the customer and validated by the project team )
- Low-Level Design ( LLD)

The Project Leader contact the Architect assigned to their circle to define the [HLD](HLD.md) document taking into account the following factors:
- Functional requirements of the project
- Possibly pre-existing architecture 
- The client's ability to exercise the new architecture (in case Agile Lab is not in charge of the run and maintenance service )
- Deployment Technical Constraints
- Agile Lab Technical Strategy
- Skill of the team that composes the project circle

The HLD is the starting point for discussion and alignment with the development team, and it is an accountability of the "architect".
The HLD should go into the following topics:
- Logical architecture
- Physical architecture
- Software architecture
- High-level data flow
- Integrations
- Performance analysis (if necessary)

The development team must now work on a Low-Level Design, where it is needed to go into details about logical flow, error handling, interface sketch, logging, patterns and all is needed to align the expectations of the customer and the project leader, to the understanding of the problem by the development team.

The LLD must be implemented in the form of Issue GitLab to form the project backlog, which will then be iteratively refined throughout the project. Issues can also be integrated with documents, schemas, and everything you think is necessary to make each development task understandable. 
The LLD in AgileLab is considered an internal phase, unless a customer is explicitly requesting it in terms of documentation.

At this stage, it is also necessary to involve the "Chief Developer" who will indicate which software components already developed in the past should be reused or can provide indications on which components could become an asset for the future.
The LLD must then be shared with the Architect and Project Leader and approved by them.

At this point, there are conditions to start the development.

# Implementation

This phase, is tipically not visible to the customer and explained in the section [Software Development](SoftwareDevelopment.md)

From an operations standpoint we enforce three type of controls along the project lifecycle:

**Delivery Control**

the Project Leader about once every two weeks holds a "tactical meeting" with the COO (Delivery Leader) to update the progress and possibly highlight issues to be addressed at operational level. From a formal point of view, the information within Gitlab must always be updated and provide all the necessary details about the status of the project. 

Every three weeks, however, there will be a governance meeting within the "Project Delivery" circle where any organizational issues within the various projects will be discussed, including resource staffing.

The Project Leader within his circle is free to organize the work and coordination methods that he reputes most appropriate and for this reason no further details will be defined on this matter, as stated in the handbook regarding the circle's properties. 

In "formal" projects, the PMO also maintains and exports to the customer a higher-level project plan drawn up with Microsoft Project and helps the Project Leader to manage the communication flow with the customer. 

**Technical Control**

At the beginning of each project, an "Architect" is assigned to agree on the system architecture and the technologies that will be used, so that customer expectations and technological consistency will be preserved.

Along the implementation phase, the Project Leader is responsible for the quality of the code and all the technical detail choices that arise daily, this is happening through a system of code review and feedbacks.
Each member of the development team should be proactive in solving problems and sharing solutions with the Project Leader and the Architect as far as they are concerned.
If the choices to be made represent a change or introduce new aspects to the system from an architectural standpoint, it will be necessary to align the Architect and produce also a new version of HLD . 

Each team member must be respectful of the Project Leader's organization and decisions, helping him to maintain the leadership and taking care about team success. 
If the Project Leader does not have enough technical leadership to make right decisions, he can use the support of the architect and the "Chief Developer".

Wrapping up in case of implementation needs the reference point is the circle "Software Development" that will provide best practices and guidelines, but that will not be in charge to provide solutions that modify or disrupt the behavior of the system. If the proposed solution changes the behavior of the system, a comparison with the architect is therefore required.
The "Software Development" circle, that should take care about software quality of the entire factory and the technical growth path of the individual developers, may require to have in-depth sessions with the project team and/or participate in the code review phases with individuals.

It is very important to distinguish between a theme of "system" and a theme of "implementation", the "software developer" can suggest system solutions (as well as others that participate in informal meetings or brainstorming sessions ), but these must always be reported and validated by the architect to be sure to take decisions with the appropriate context and vision.

**Financial Control**

The costs of Agile Lab projects are kept under control with a budgeting tool: elapseit.
Company staff will need to fill out allocation timesheets directly on this tool on a weekly basis, to allow appropriate cost tracking and reporting.







