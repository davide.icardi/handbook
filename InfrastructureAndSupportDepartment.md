# Infrastructure and Support Dept.

The infrastructure/SRE team aims to design, implement and made operational infrastructure and provide support for applications in production in mission critical environments (24/7).

# Roles

1. **Team Leader:** is responsible for the customer and the activities that are carried out.
2. **Circle Leader**: he is responsible for tasks, tickets and events that occur within his area of responsibility (Circles).
3. **Member**: has the task to remain informed and operational within a circle and to be active in case of need (pro-active, on demand triggered by circle leader)

# Circle

The team is organized into circles, which operate independently and are oriented to take care of the activities related to a specific customer/project.

The **team leader** is responsible for reporting the status of the entire team (all circles) to the CTO and making critical and business decisions about the circles with it.

Within a circle there is a **circle leader** who is in charge of keeping track of what is happening and organizing activities; in addition to the circle leader there are as many **members** as necessary to employ in relation to the circle activities.

The **philosophy** behind this type of organization is to increase the responsibility of the team and to help the people who are part of it to have a higher level of involvement and perception of responsibility. In addition, everyone is responsible for something and this helps to grow every element of the team without limits of seniority.

# Team feedback loop

The team uses various tools to generate and receive feedback. 

The feedback tools are differentiated by purpose and effectiveness and are, as we read in SRE literature, used according to the type of feedback you want to give and the urgency of the event you have to manage.

An example of tools used can be mail, GitLab/JIra (e.g. Issue and task tracking), Slack and a ticket system (e.g. Jira Service Desk).

# Engagement

**External**

The times and methods of engagement are formalized by Agile Lab towards customers in terms of SLAs and mode of engagement (eg. ticket, email, call).
The team adapts to the needs of customers and operates in accordance with the agreements managing the interventions and the related communication in an effective and efficient manner.

**Internal**

Internally, the team is stimulated and hired in relation to the circle of belonging **using time slots** with other teams or with people who need support. This allows you to organize work activities and give the right priorities.

We want to minimize the typical "do yuo have a minute?" phenomenon.

**Practice**

The **time slot** is typically sent via calendar/appointment tool and allows the recipient to accept, refuse or propose a different time.
The time slot is asked to a **Member** of the circle who has the right to accept or refuse if busy. If the slot is asked to the **Circle leader** it can accept or refuse, but in case of refusal must still **handle the request** with the help of a member of the circle.

**In case of strong criticality**, the engagement can / must be made by mobile phone to a member of the circle concerned and it can not refuse to manage the engagement even if busy with other activities.

# Documentation

For each project/infrastructure a technical documentation is produced that is written in markdown format and versioned through a **Git** repository ({customer}.{project}.DevOps); this documentation contains:

* an exhaustive infrastructural map of the solution (e.g. made by draw.io)
* hosts list, connection matrix, network configuration
* a detailed set of commands (command misc) to act on the various parts of the infrastructure
* where are the backups
* describe a procedure for recovering and restoring the backup during a critical event
* which cronjobs are present and how they are activated (host, activation time)
* how to create a new user to allow access to the infrastructure
* where the secrets are stored

We have seen where to put the documentation that accompanies the projects and infrastructure; now we go to define where to document the incidents, post-mortem and all searchable text.

This kind of documentation must be inserted and collected on the internal system of **notes** that allows you to collect and write in a complete way everything that happens during an incident.
All standard sections are listed on our internal note system in a special section called **Infra Standards**.

# Working in production

A production environment is not a free environment. It is bound to business related operations, so it must be protected and used according to common sense rules and with care.

I would like to report here the procedure that I would like to be followed to use production for extraordinary activities. Just 3 easy rules.

•	You can not give demo/test in production; you must use test/pre prod/demo environments.
•	If you have access to a production environment because you are involved in support shifts or because you carry out checks but you are not part of the infrastructure team, I ask you to notify the Circle leader of the activity by specifying what you are doing (activities abstract and timeframe).
•	If you produce something that is not related to the operation of the application in production, ie temporary files, temporary folders (FS or HDFS), tables, topics, collection these must be deleted at the end of the activity.